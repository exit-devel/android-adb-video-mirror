Linux script to capture Android (LineageOS preferably) image screenshot sequences, both from TWRP and Android native mode, using ADB.
This script use ffmpeg (for TWRP) or screencap (for native Android mode) and creates image files that are read sequentially by the browser reading "adb-screen-at.html" page.

Dependencies:

ADB
FFMPEG
