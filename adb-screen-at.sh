#!/bin/bash
# -----------------------------------------------------------------------------
# ADB Screen
#
# Modulo script:    adb-screen-at.sh
# Versione:         1.3 - 05/05/2019
#
# Realizzato dal supporto tecnico di Exit - www.3x1t.org
# Davide Marchi - Fabrizio Cagnina
# -----------------------------------------------------------------------------

# Flag di determinazione sistema Android / TWRP ("A" / "T")
DEVICE_OS_CODE=""

# Avvio server ADB
# adb start-server

# Verifica e creazione cartella di lavoro script (locazione RAM)
if [ ! -d /tmp/exit-adb-screen ]; then
    mkdir /tmp/exit-adb-screen
fi

# Copia preliminare immagini default.
cp -f no-device-p.png /tmp/exit-adb-screen/a-screen.png
cp -f no-device-l.png /tmp/exit-adb-screen/t-screen-l.jpg
cp -f no-device-p.png /tmp/exit-adb-screen/t-screen-p.jpg

# Determinazione device Android / TWRP.
if adb devices | grep -q "recovery"; then
    DEVICE_OS_CODE="T"
    echo "Rilevato device TWRP"
else
    DEVICE_OS_CODE="A"
    echo "Rilevato device Android / Lineage"
fi

# Ciclo script.
echo ADB Screen started...
while true
do
    # -------------------------------------------------------------------------
    # Android
    # -------------------------------------------------------------------------
    # if adb shell screencap -p > $SCRIPT_WORK_PATH/$ANDROID_IMG_GET; then
    # if adb shell screencap -p | sed 's/\r$//' > $SCRIPT_WORK_PATH/$ANDROID_IMG_GET; then
    # -------------------------------------------------------------------------
    if [ "$DEVICE_OS_CODE" == "A" ]; then
        if adb exec-out screencap -p > /tmp/exit-adb-screen/a-get-screen.png; then
            cp -f /tmp/exit-adb-screen/a-get-screen.png /tmp/exit-adb-screen/a-screen.png
        fi
    elif [ "$DEVICE_OS_CODE" == "T" ]; then
        if adb pull /dev/graphics/fb0 /tmp/exit-adb-screen/fb0; then

            # Conversione immagine formato landscape
            ffmpeg -y -vcodec rawvideo -f rawvideo -pix_fmt rgba -s 1280x720 -i /tmp/exit-adb-screen/fb0 -f image2 -vcodec mjpeg -vframes 1 /tmp/exit-adb-screen/fb0l.jpg
            cp -f /tmp/exit-adb-screen/fb0l.jpg /tmp/exit-adb-screen/t-screen-l.jpg

            # Conversione immagine formato portrait
            ffmpeg -y -vcodec rawvideo -f rawvideo -pix_fmt rgba -s 720x1280 -i /tmp/exit-adb-screen/fb0 -f image2 -vcodec mjpeg -vframes 1 /tmp/exit-adb-screen/fb0p.jpg
            cp -f /tmp/exit-adb-screen/fb0p.jpg /tmp/exit-adb-screen/t-screen-p.jpg

        fi
    fi

done

# Arresto server ADB
# adb stop-server

